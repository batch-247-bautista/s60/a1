import React from 'react';
import { Link } from 'react-router-dom';
import { Dropdown, DropdownButton } from 'react-bootstrap';

const AdminDashboard = () => {
  return (
    <div>
      <h1>Admin Dashboard</h1>
      <DropdownButton title="Select action">
        <Dropdown.Item as={Link} to="/admin/add-product">Add Product</Dropdown.Item>
        <Dropdown.Item as={Link} to="/admin/inventory">Inventory of Products</Dropdown.Item>
        <Dropdown.Item as={Link} to="/admin/delete-product">Delete Products</Dropdown.Item>
      </DropdownButton>
    </div>
  );
};

export default AdminDashboard;

