import { Card, Row, Col } from 'react-bootstrap';

export default function Highlight() {
  return (
    <Row className="mt-3 mb-3">
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Order Anywhere</h2>
                    </Card.Title>
                    <Card.Text>
                        You can place orders on your phone, computer and any of Shoes Mio branches nationwide!
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Check out the latest shoe lines realease by brands!</h2>
                    </Card.Title>
                    <Card.Text>
                        Orders and purchases are subject to stock availability.  Confirmation of stock will be determined upon checkout.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3">
                <Card.Body>
                    <Card.Title>
                        <h2>Be Stylish. Get the latest trend in shoes!</h2>
                    </Card.Title>
                    <Card.Text>
                        Reward yourself!  We have lots of lifestyle and active wear shoes of different brands and models for your choosing.
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    </Row>
  );
}

const ProductCard = ({ product }) => {
  return (
    <Card className="product-card p-3">
      <Card.Img variant="top" src={product.image} />
      <Card.Body>
        <Card.Title>{product.title}</Card.Title>
        <Card.Text>{product.description}</Card.Text>
        <Card.Text>Price: ${product.price}</Card.Text>
        <button className="btn btn-primary">Order Now!</button>
      </Card.Body>
    </Card>
  );
};

